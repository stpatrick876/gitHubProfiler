import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileComponent } from './profile.component';
import { FormsModule } from '@angular/forms';
import { GitApiService } from '../../services/github/git-service.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/from';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  let searchInput: any;
  let _apiService: GitApiService;
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientTestingModule],
      providers: [GitApiService],
      declarations: [ ProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    searchInput = fixture.debugElement.nativeElement.querySelector('[name=username]');

    _apiService = TestBed.get(GitApiService);

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call searchUser on key up ', () => {
    spyOn(component, 'searchUser').and.callThrough();
    searchInput.dispatchEvent(new Event('keyup'));
    fixture.detectChanges();
    expect(component.searchUser).toHaveBeenCalled();
    expect(component.user).toBeNull();
  });

  it('should set user', () => {

    spyOn(_apiService, 'searchUser').and.callThrough();
    spyOn(_apiService, 'getUser').and.returnValue(Observable.of({public_repos: 2}));
    spyOn(_apiService, 'getRepos').and.returnValue(Observable.from([{
      name: 'repo 1'
    },
      {name: 'repo 2'}
      ])
    );


    fixture.whenStable().then(() => {
      searchInput.value = 'kemar-stpatrick876';
      fixture.detectChanges();

      searchInput.dispatchEvent(new Event('input'));
      fixture.detectChanges();
      expect(component.username).toBeDefined();

      searchInput.dispatchEvent(new Event('keyup'));
      fixture.detectChanges();
      expect(_apiService.searchUser).toHaveBeenCalledWith('kemar-stpatrick876');

      expect(component!.user!.public_repos).toBe(2);
      expect(component!.repos.length).toBe(2);
    });

  });


});
