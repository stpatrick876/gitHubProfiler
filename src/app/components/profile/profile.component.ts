import { Component, OnInit } from '@angular/core';
import { GitApiService } from '../../services/github/git-service.service';
import {
  animate, stagger, state, style, transition, trigger, query, keyframes,
  animateChild, group
} from '@angular/animations';
@Component({
  selector: 'profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  animations: [
   trigger('listRolesState',  [
    transition('navClosed => navOpened', [

      query('nav ul li', stagger('100ms', [
        style({ opacity: 0, transform: 'translateX(-100%)'}),
        animate('140ms 10ms ease', keyframes([
          style({opacity: 0.1, transform: 'translateX(-100%)', offset: 0}),
          style({opacity: .5, transform: 'translateX(-50%)', offset: 0.3}),
          style({opacity: 1, transform: 'translateX(0)', offset: 1.0}),
        ]))]), {optional: true}),

       query('@navItemState', animateChild(), {optional: true})
    ])
    ,
    transition('navOpened => navClosed', [


      query('nav ul li', stagger('100ms', [
        animate('140ms 10ms ease', keyframes([
          style({opacity: 1, transform: 'translateX(0%)', offset: 0}),
          style({opacity: .5, transform: 'translateX(50%)',  offset: 0.3}),
          style({opacity: 0, transform: 'translateX(100%)', display: 'none',  offset: 1.0}),
        ]))]), {optional: true})
    ])
  ])]

})
export class ProfileComponent {
  username: string;
   user: any;
   repos: any;
  constructor(private _apiService: GitApiService) { }

  searchUser() {
    if (!this.username) {
      this.user = null;
    }
   this._apiService.searchUser(this.username);
    this._apiService.getUser().subscribe(user => this.user = user);
    this.repos = this._apiService.getRepos();
  }

}
