import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class GitApiService {
  private api = 'http://api.github.com/users';
   private config = {
           username: '',
          clientId: 'f510dbe738199bd9eed0',
          clientSecret: 'f67f1974353d8c27fb9283afbb62b5ff8db957af'
   };
  constructor(private http: HttpClient) {
  }


  getUser() {
    return this.http.get(`${this.api}/${this.config.username}?client_id=${this.config.clientId}&client_secret=${this.config.clientSecret}`);
  }

  getRepos() {
    return this.http.get(`${this.api}/${this.config.username}/repos?client_id=${this.config.clientId}&client_secret=${this.config.clientSecret}`);
  }

  searchUser(username: string) {
    this.config.username = username;

  }

}
