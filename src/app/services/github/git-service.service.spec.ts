import { TestBed, inject } from '@angular/core/testing';
import { GitApiService } from './git-service.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';


describe('GitService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [GitApiService ]
    });
  });

  it('should be created', inject([GitApiService], (service: GitApiService) => {
    expect(service).toBeTruthy();
  }));
});
